class ExtrapolateEdgeByStep extends Step {

  protected PVector cvxHullPnt;
  protected PVector cvxHullPnt2;
  protected PVector currentMostLeftPnt;
  protected float currentMostLeftSin = 0;
  protected PVector currentMaxLeftDistPnt;
  protected float currentMaxLftDist = 0;
  protected int indxCorner;
  protected int numberOfChanges = 0;

  ExtrapolateEdgeByStep(ConvexHull cvxHull, int indxCrn) {
    super(cvxHull, 0);
    this.indxCorner = indxCrn; //<>//
    this.cvxHullPnt = convexHull.getPoint(indxCorner);
    this.cvxHullPnt2 = convexHull.getPoint(indxCorner+1);
  }
  
  private void reset() {
    currentMostLeftPnt = new PVector();
    currentMostLeftSin = 0;
    currentMaxLeftDistPnt = new PVector();
    currentMaxLftDist = 0;
    this.cvxHullPnt = convexHull.getPoint(indxCorner);
    this.cvxHullPnt2 = convexHull.getPoint(indxCorner+1);
    this.convexHullIterator = convexHull.listIterator(0);
    this.pntsToProcessIterator = pntsToProcess.listIterator();
  }

  public boolean update() {

    if (pntsToProcessIterator.hasNext()) {
      
      beingPrcsdPoint = pntsToProcessIterator.next();
      
      float algbrEdgeDist = EnumSide.algbrEdgeDist(cvxHullPnt, cvxHullPnt2, beingPrcsdPoint);
      float angle = EnumSide.getSin(cvxHullPnt, cvxHullPnt2, beingPrcsdPoint);
      
      if (algbrEdgeDist > currentMaxLftDist  && angle > currentMostLeftSin) {
        numberOfChanges++;
        int indxDistToReplace = convexHull.indexOf(currentMaxLeftDistPnt);
        int indxAnglToReplace = convexHull.indexOf(currentMostLeftPnt);
        
        if (indxDistToReplace == -1 && indxAnglToReplace == -1) {
          convexHull.add(indxCorner+1, beingPrcsdPoint);
        } else if (indxDistToReplace != indxAnglToReplace) {
          convexHull.remove(currentMaxLeftDistPnt);
          convexHull.set(indxAnglToReplace, beingPrcsdPoint);
        } else {
          //convexHull.set(indxDistToReplace, beingPrcsdPoint);
          convexHull.set(indxAnglToReplace, beingPrcsdPoint);
        }
        currentMaxLftDist = algbrEdgeDist;
        currentMaxLeftDistPnt = beingPrcsdPoint;
        
        currentMostLeftSin = angle;
        currentMostLeftPnt = beingPrcsdPoint;
        
      } else if (algbrEdgeDist > currentMaxLftDist && angle < currentMostLeftSin) {
        numberOfChanges++;
        int indxDistToReplace = convexHull.indexOf(currentMaxLeftDistPnt); //<>//
        int currentMostLeftIndex = convexHull.indexOf(currentMostLeftPnt);
        
        if (indxDistToReplace == currentMostLeftIndex) {
          convexHull.add(currentMostLeftIndex+1, beingPrcsdPoint); //<>//
        } else {
          convexHull.set(currentMostLeftIndex+1, beingPrcsdPoint);           //<>//
        }
        
        currentMaxLftDist = algbrEdgeDist;
        currentMaxLeftDistPnt = beingPrcsdPoint;
        
      } else if (angle > currentMostLeftSin && algbrEdgeDist < currentMaxLftDist) {
        numberOfChanges++;
        int currentMaxDistIndex = convexHull.indexOf(currentMaxLeftDistPnt);
        int indxAngleToReplace = convexHull.indexOf(currentMostLeftPnt);
        
        if (indxAngleToReplace == currentMaxDistIndex) {
          convexHull.add(currentMaxDistIndex, beingPrcsdPoint);
        } else {
          convexHull.set(indxAngleToReplace, beingPrcsdPoint);          
        } 
        
        currentMostLeftSin = angle;
        currentMostLeftPnt = beingPrcsdPoint;      
        
      }
    } else {
      if (numberOfChanges == 1) {
        indxCorner++;
      }
      indxCorner++;
      if (indxCorner < convexHull.size()) {
        this.reset();
        return false;
      } else {
        return true;
      }
    }
    return false;
  }

  public void show() {
    super.show();
    stroke(0, 126, 255);
    strokeWeight(5);
    try {
      connect(cvxHullPnt, cvxHullPnt2);
      connect(cvxHullPnt, beingPrcsdPoint);
    } 
    catch (NullPointerException e) {
    }
  }
}
