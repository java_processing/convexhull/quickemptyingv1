class EmptyInsideByStep extends Step {

  protected PVector cvxHullPnt;
  protected PVector cvxHullPnt2;
  protected boolean nextPoint = true;
  protected int indxCorner;
  protected int numberRemoved;

  EmptyInsideByStep(ConvexHull cvxHull, int idxCrn) {
    super(cvxHull, idxCrn);
    this. indxCorner = idxCrn;
  } //<>//

  public boolean update() {
    if (pntsToProcessIterator.hasNext() || !nextPoint) {

      cvxHullPnt = convexHullIterator.next();
      int indx = convexHullIterator.nextIndex();
      cvxHullPnt2 = convexHull.getPoint(indx);

      if (nextPoint) {
        beingPrcsdPoint = pntsToProcessIterator.next();
      }
      nextPoint = false;

      EnumSide side = EnumSide.whichSide(cvxHullPnt, cvxHullPnt2, beingPrcsdPoint);

      if (side == EnumSide.Left) {
        nextPoint = true;
      }
      if ((!convexHullIterator.hasNext()) || nextPoint) {
        convexHullIterator = convexHull.listIterator(indxCorner);
        if (!nextPoint) {
          unvalidPrcsdPnt.add(beingPrcsdPoint);
          pntsToProcessIterator.remove();
          numberRemoved++;
        }
        nextPoint = true;
      } else { 
        nextPoint = false;
      }
      return false;
    } else {
      if (indxCorner < convexHull.size()) {
        convexHull.setStep(new ExtrapolateEdgeByStep(convexHull, indxCorner));
        return false;
      } else {
        return true;
      }
    }
  }

  public void show() {
    super.show();
    stroke(0, 126, 255);
    strokeWeight(5);
    try {
      connect(cvxHullPnt, cvxHullPnt2);
      connect(cvxHullPnt, beingPrcsdPoint);
    } 
    catch (NullPointerException e) {
    } //<>//
  }
}
