import noiseAlgorithms.RandomGenerator;
import java.util.ListIterator;
import java.util.Collection;
import java.util.Collections;
import java.util.Arrays;
import java.util.ArrayList;

int nbPoints = 1000;   // for the graham's scan, it may doesn't work depending on the number of points and the point generation probability law
int seed = 4;
PointCloud pntCloud;
ConvexHull cvxHull;
int in = 0;
boolean isAnimated = true;
EnumRandom typeRand = EnumRandom.Uniform; //Gaussian / Uniform

void setup () {
  //fullScreen();
  frameRate(Float.POSITIVE_INFINITY);
  size(950, 950);
  //pntCloud = new PointCloud(nbPoints);
  pntCloud = new PointCloud(nbPoints, seed, typeRand);
  //pntCloud.add(new SortedPoint(300,230));
  cvxHull = new ConvexHull((PointCloud) pntCloud.clone(), isAnimated);
}

void draw() {
  translate(width/2, height/2);
  noFill();

  //if (in%1 == 0 ) {println(frameRate);}
  

  if (in%1 == 0 ) {
    cvxHull.update();
    cvxHull.show();
  }
  in++;
}

public void connect (PVector a, PVector b) {
  line(a.x, a.y, b.x, b.y);
}
