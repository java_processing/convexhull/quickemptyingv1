class FindExtremasByStep extends Step {

  private float minX = Integer.MAX_VALUE;
  private float maxX = Integer.MIN_VALUE;
  private float minY = Integer.MAX_VALUE;
  private float maxY = Integer.MIN_VALUE;
  private float X, Y;

  FindExtremasByStep(ConvexHull cvxHull) {
    super(cvxHull, 0);
  }

  public boolean update() {

    if (pntsToProcessIterator.hasNext()) {
      beingPrcsdPoint = pntsToProcessIterator.next();
      X = beingPrcsdPoint.x;
      Y = beingPrcsdPoint.y;

      if (X < minX) {
        minX = X;
        convexHull.noErrorSet(0, beingPrcsdPoint);
      }
      if (Y < minY) {
        minY = Y;
        convexHull.noErrorSet(1, beingPrcsdPoint);
      }
      if (X > maxX) {
        maxX = X;
        convexHull.noErrorSet(2, beingPrcsdPoint);
      }
      if (Y > maxY) {
        maxY = Y;
        convexHull.noErrorSet(3, beingPrcsdPoint);
      }
    } else {
      for (PVector extremPoint : convexHull) {
        pntsToProcess.remove(extremPoint);
      }
      convexHull.setStep(new EmptyInsideByStep(convexHull, 0));
    }
    return false;
  }

  public void show() {
    super.show();

    stroke(255, 126, 0);
    strokeWeight(8);
    for (PVector extrPoint : convexHull) {      
      point(extrPoint.x, extrPoint.y);
    }
  }
}
